from django.urls import path
from main import views
from .decorators import check_recaptcha

app_name = 'main'
urlpatterns = [
    path('send_email/', check_recaptcha(views.send_email), name='send_email'),
    path('', views.basic, name='basic')
]
