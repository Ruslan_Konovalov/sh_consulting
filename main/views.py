from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView
from django.core.exceptions import ObjectDoesNotExist

from django.conf import settings
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse, reverse_lazy
from django.shortcuts import render, redirect
from django.utils import timezone
from django.contrib import auth
from django.contrib.auth.models import AnonymousUser
from django.views import View
from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormView, DeleteView, UpdateView
from django.core.cache import cache
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.core.mail import send_mail, BadHeaderError

from datetime import datetime, timedelta
import requests
import json
import time
import os

def basic(request):
    if auth.get_user(request) == AnonymousUser():
        return render(request, 'main/index.html')
    else:
        return render(request, 'main/index.html')
        # username = auth.get_user(request).username
        # return render(request, 'main/index.html', {'username':username})


def send_email(request):
    name = request.POST.get('name')
    from_email = request.POST.get('mail')
    phone = request.POST.get('phone')
    subject = request.POST.get('topic')
    message = request.POST.get('text')

    if subject and message and phone and name and from_email and request.recaptcha_is_valid:
        try:
            send_mail(subject,
            "Message: "+message+"; Phone: "+phone+"; Name: "+name+"; Email: "+from_email,
            'Inbuconsalta@yandex.ru',
            ['Inbuconsalta@yandex.ru']
            )
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        except Exception as e:
            print(e)
            return JsonResponse({'result': 'ERROR'})
        return redirect('/')
        # return HttpResponseRedirect('/contact/thanks/')

    else:
        # In reality we'd use a form class
        # to get proper validation errors.
        return HttpResponse('Вы точно заполнили все поля в форме? Проверьте ещё раз.')
