$(function () {


  $('.gallery').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1800,
        settings: {
          slidesToShow: 4,
        }
      },
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });


  function set_active_nav(nav) { // class 'active_menu' get white text and arrow to nav
    $('.menu > a').removeClass('active_menu');
    nav.addClass('active_menu');
  }


  $('.gallery .slick-arrow').text(' ')
  $('.menu > a').click(function () {
    set_active_nav($(this))
  });



  new Waypoint({
    element: $('.about'),
    handler: function() {
      set_active_nav($('#about_link'))
    },
    offset: 160
  });
  new Waypoint({
    element: $('.services'),
    handler: function() {
      set_active_nav($('#services_link'))
    },
    offset: 160
  });
  new Waypoint({
    element: $('.commerce'),
    handler: function() {
      set_active_nav($('#commerce_link'))
    },
    offset: 160
  });
  new Waypoint({
    element: $('.marketing'),
    handler: function() {
      set_active_nav($('#marketing_link'))
    },
    offset: 160
  });
  new Waypoint({
    element: $('.gallery'),
    handler: function() {
      set_active_nav($('#gallery_link'))
    },
    offset: 160
  });
  new Waypoint({
    element: $('.contact_us'),
    handler: function() {
      set_active_nav($('#contacts_link'))
    },
    offset: 260
  });

});
